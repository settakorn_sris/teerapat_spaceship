﻿using System.Collections;
using System.Collections.Generic;
using SpaceShip;
using UnityEngine;
using System;
using Manager;

public class BossSpaceship : BaseSpaceShip, IDamagable
{
    public event Action OnExploded;
    [SerializeField] private double fireRate = 1;
    private float fireCounter = 0;
    private void Awake()
    {
        Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
        Debug.Assert(gunPosition != null, "gunPosition cannot be null");
    }
    public void Init(int hp, float speed)
    {
        base.Init(hp, speed, defaultBullet);
    }

    public void TakeHit(int damage)
    {
        Hp -= damage;
        if (Hp > 0)
        {
            return;
        }
        Explode();
    }

    public void Explode()
    {
        Debug.Assert(Hp <= 0, "HP is more than zero");
        gameObject.SetActive(false);
        OnExploded?.Invoke();
        SoundManager.Instance.Play(audioSource, SoundManager.Sound.Enemydeath);
    }

    public override void Fire()
    {
        Debug.Log("test");
        fireCounter += Time.deltaTime;
        if (fireCounter >= fireRate)
        {
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Enemyfire);
            var bullet1 = Instantiate(defaultBullet, gunPosition[0].position, Quaternion.identity);
            var bullet2= Instantiate(defaultBullet, gunPosition[1].position, Quaternion.identity);
            bullet1.Init(Vector2.down);
            bullet2.Init(Vector2.down);
            fireCounter = 0;
        }
    }

}

