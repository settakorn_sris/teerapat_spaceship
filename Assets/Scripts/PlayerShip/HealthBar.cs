﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;

public class HealthBar : Singleton<GameManager>
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private int playerSpaceshipHp;


    public Slider slider;

  
    public void MaxHealth()
    {
        slider.maxValue = playerSpaceshipHp;
        slider.value = playerSpaceshipHp; 
    }

     public void SetHealth()
    {
        slider.value = playerSpaceshipHp;
    }
}