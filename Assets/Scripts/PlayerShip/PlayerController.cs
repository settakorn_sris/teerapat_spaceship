﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using Spaceship;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceShip playerSpaceship;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float xMin;
        private float xMax;
        private float yMin;
        private float yMax;
        private float padding = 1.5f;
        
        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceship.Fire();
        }

        public void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var inputVelocity = movementInput * playerSpaceship.Speed;
            
            var newPosition = transform.position;
            newPosition.x = transform.position.x + inputVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inputVelocity.y * Time.smoothDeltaTime;

            newPosition.x = Mathf.Clamp(newPosition.x, xMin, xMax);
            newPosition.y = Mathf.Clamp(newPosition.y, yMin, yMax);

            transform.position = newPosition;
        }

        public void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null,"Main camera cannot be null");

            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");
            
            var offset = spriteRenderer.bounds.size;                  
            xMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / padding;
            xMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / padding;
            yMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / padding;
            yMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / padding;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
