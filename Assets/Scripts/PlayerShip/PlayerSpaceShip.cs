﻿using System;
using Manager;
using SpaceShip;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;
        
       
        private void Awake()
        {
            Debug.Assert(defaultBullet != null,"defaultBullet cannot be null");
            Debug.Assert(gunPosition != null,"gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition[0].position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Playerfire);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Playerdeath);
        }

      
    }
}